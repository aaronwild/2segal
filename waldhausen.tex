\documentclass{shortnotes}

%%
\usepackage{style-shortnotes}
%%
\usepackage{custommath_gen}
\usepackage{category-names}
\usepackage{custommath_font}
\usepackage{custommath_gerbe}


\newcommand{\pushoutsign}{\hspace{0.2ex}\tikz[baseline=(po.base)]{\draw (0,0) ++ (-0.75ex,-0.75ex) -- ++(0ex,1.5ex) -- ++ (1.5ex,0ex);\node at (0.6ex,-0.6ex) {.};\node (po) at (0,0) {\phantom{x}};}}

\usetikzlibrary{shapes.geometric}
\usepackage{gitinfo2}
%%
\title{\normalsize{Waldhausen $S$-construction}}
\date{}
\author{\normalsize{Aaron Wild}\footnote{\url{aaron.wild@posteo.net}. This is version \gitAbbrevHash{} of this document. An up-to-date version can be found at \url{aaronwild.gitlab.io/writing}}}
\abstracttext{
  These are notes I prepared for a talk in the seminar \enquote{Higher Segal Spaces} in the winter term 2023/24.
  It is mostly a summary of \cite[\S 2.4]{dyckerhoff-kapranov}, with some additional insights from \cite{dyckerhoff-lectures-on-hall-algebras}.
I would like to thank Dr. J. Flake, Prof. C. Stroppel and Ferdinand Wagner for helping me prepare for this talk.
All the pictures of the Waldhausen construction are taken and modified from Ferdinand's manuscript \cite{k-theory-lecture-notes} (which also contains a lot of great material on the Waldhausen construction for stable $\infty$-categories).
Let me also mention that the talks by both Dyckerhoff and Kapranov on Higher Segal Spaces (as linked on the nLab article) were really helpful while preparing.
}
\bibliography{lit.bib}
\begin{document}
\maketitlepage
\thispagestyle{empty}
\vspace{.5cm}
\section{Proto-exact categories}
\begin{numtext}
  Recall the definition of the $K_0$-theory of a ring $R$:
  Denote by $\modcat(R)^{\omega}$ the category of finitely generated projective $R$-module.
  Then $K_0(R)$ is defined as the group completion of  the monoid $\pi_0(\core(\modcat(R)^{\omega}))/\sim$, where $\pi_0(\core(\modcat(R)^{\omega}))$ is the set of isomorphism classes in $\modcat(R)^{\omega}$, and the equivalence relation is given by $[V] = [V^{\prime}] + [V^{\prime\prime}]$ for every short-exact sequence of the form
  \[
    \begin{tikzcd}[column sep = small]
      0
      \ar{r}
      &
      V^{\prime}
      \ar{r}
      &
      V
      \ar{r}
      &
      V^{\prime\prime}
      \ar{r}
      &
      0
    \end{tikzcd},
  \]
  and the monoid structure comes from taking direct sums of extensions.
  Note that $\modcat(R)^{\omega}$ is not an abelian category\footnote{it does not even have all cokernels (e.g. $\zz/n\zz$ is not projective)}, but rather an \emph{exact category}.
  This is an additive category $\ecat$, together with a class $E$ of \enquote{extensions}, i.e. composable morphisms $\begin{tikzcd}[column sep = small, cramped]M' \ar{r} & M \ar{r} & M''\end{tikzcd}$, where the first morphism $M'\to M$ is called an \emph{admissible monomorphism}, and the second morphism $M \to M''$ is called an \emph{admissible epimorphisms}.
  We will not recall the axioms, but at least a prime source of examples:
  Let $\acat$ be an abelian category, and $\ecat \sse \acat$ a strictly full subcategory that is closed under extensions.
  Then we can take $E$ to be the class of all composable morphisms $\begin{tikzcd}[column sep = small, cramped]M' \ar{r} & M \ar{r} & M''\end{tikzcd}$ such that
  \[
    \begin{tikzcd}[column sep = small]
      0
      \ar{r}
      &
      M' \ar{r} & M \ar{r} & M''
      \ar{r}
      &
      0
    \end{tikzcd}
  \]
  is exact in $\acat$.
\end{numtext}

\begin{defn}
  A \emph{proto-exact category} is a triple $(\ecat,\adm,\ade)$, with $\ecat$ a category, and $\adm,\ade \sse \mor(\ecat)$ classes of morphisms, subject to the following conditions:
  \begin{enumerate}
  \item
    $\ecat$ is pointed, i.e. has an object $0$ that is both initial and final.
    Any morphism $0\to A$ is in $\adm$.
    Any morphism $A \to 0$ is in $\ade$.
  \item
    Both $\adm$ and $\ade$ are closed under composition and contain all isomorphism.
  \item
    Let
    \[
      \begin{tikzcd}
        A^{\prime}
        \ar[hookrightarrow]{r}[above]{i'}
        \ar[twoheadrightarrow]{d}[left]{p'}
        &
        B^{\prime}
        \ar[twoheadrightarrow]{d}[right]{p}
        \\
        A
        \ar[hookrightarrow]{r}[below]{i}
        &
        B
      \end{tikzcd}
    \]
    be a commutative diagram in $\she$, with $i,i^{\prime}$ in $\adm$ and $j,j^{\prime}$ in $\ade$.
    Then this diagram is cartesian if and only if it is cocartesian.
  \item
    Any diagram of the form $\begin{tikzcd}[column sep = small, cramped] A \ar[hookrightarrow]{r}[above]{i} & B \ar[twoheadleftarrow]{r}[above]{p} & B^{\prime} \end{tikzcd}$ in $\she$ with $i \in \adm$ and $p\in \ade$ can be completed to a bicartesian square of the form
    \[
      \begin{tikzcd}
        A^{\prime}
        \ar[hookrightarrow,dashed]{r}[above]{i'}
        \ar[twoheadrightarrow,dashed]{d}[left]{p'}
        &
        B^{\prime}
        \ar[twoheadrightarrow]{d}[right]{p}
        \\
        A
        \ar[hookrightarrow]{r}[below]{i}
        &
        B
      \end{tikzcd}
    \]
    with $i^{\prime}\in \adm$ and $p^{\prime}\in \ade$.
  \item
    Dually, any diagram of the form $\begin{tikzcd}[column sep = small, cramped] A \ar[twoheadleftarrow]{r}[above]{p'} & A' \ar[hookrightarrow]{r}[above]{i'} & B^{\prime} \end{tikzcd}$  can be completed to a bicartesian square of the form
   \[
     \begin{tikzcd}
       A^{\prime}
       \ar[hookrightarrow]{r}[above]{i'}
       \ar[twoheadrightarrow]{d}[left]{p'}
       &
       B^{\prime}
       \ar[twoheadrightarrow,dashed]{d}[right]{p}
       \\
       A
       \ar[hookrightarrow,dashed]{r}[below]{i}
       &
       B
     \end{tikzcd}
   \]
   with $i\in \adm$ and $p\in \ade$.
  \end{enumerate}
  We call the class $\adm$ the class of \emph{admissible monomorphisms}, and $\ade$ the class of \emph{admissible epimorphisms}.
\end{defn}

\begin{example}
  \leavevmode
  \begin{enumerate}
  \item
    Any Quillen exact category is proto-exact, with same classes of admissible monomorphisms and epimorphisms.
    So in particular, for any extension-closed full subcategory $\ecat \sse \acat$ of an abelian category $\acat$, we have that $\ecat$ is proto-exact, and any abelian category itself is proto-exact.
    So $\repcat_k(G)$ for $G$ a group and $k$ a field, is proto-exact.
  \item
    Let $\pset$ be the category of pointed sets, i.e. objects of $\pset$ are pointed sets $(S,\ast_{S})$ and morphisms in $\pset$ are base-point preserving maps.
    This becomes proto-exact, for $\adm$ consisting of all injections of pointed sets, and $\ade$ consisting of surjections $p \mc (S,\ast_S) \to (T,\ast_T)$ that satisfy $\abs{p^{-1}(t)} = 1$ for $t\neq \ast_{T}$.
    Note that we need this condition for the equivalence $\text{cartesian} \Leftrightarrow \text{cocartesian}$ to hold:
    \begin{inexample*}
      As a counterexample, let $A = C = \Set{\ast}$, $B = \Set{\ast,\bullet_1,\bullet_2}$ and $D = \Set{\ast,\bullet}$.
      Consider the diagram of pointed sets
      \[
        \begin{tikzcd}
          A
          \ar{r}
          \ar{d}
          &
          B
          \ar{d}
          \\
          C
          \ar{r}
          &
          D
        \end{tikzcd}
      \]
    where the only non-obvious map is given by
    \[
      B \to D,~ \ast\mapsto \ast, ~ \bullet_1,\bullet_2 \mapsto \bullet.
    \]
    Then this is cartesian, but not cocartesian --- we have $A \sqcup_C B \cong B$ for the pushout (only the $\ast$'s get identified).
    \end{inexample*}
  \item
    Let $\ecat$ be proto-exact, and $\ccat$ any category.
    Then the functor category $\fun(\ccat,\ecat)$ is again proto-exact, for the \enquote{pointwise} definition of admissible monomorphism and epimorphisms.
    By this we mean that a natural transformation $\eta \mc F' \to F$ of functors $F,F'\mc \ccat \to \ecat$ is an admissible monomorphism/epimorphisms if and only if $\eta(x) \mc F(x) \to F'(x)$ is an admissible monomorphism/epimorphisms for all $x\in \ccat$.
    The reason that this works is that in functor categories, pullbacks and pushouts can be computed componentwise.\footnote{This is again similar to abelian categories: if $\acat$ is abelian and $\ccat$ any category, then $\fun(\ccat,\acat)$ is again abelian.}
    \begin{inrem*}
      We recover in this way that $\repcat_k(G)$ is proto-exact, since it can be described as a functor category via
      \[
        \repcat_k(G)
        \cong
        \fun(BG,\modcat(k)^{\omega}),
      \]
      for $BG$ the category with one object $x$ and automorphisms given by $G$.
      But we also get that the category $\repcat_{\ff_1}(G)$ of finite pointed sets with $G$-action is proto-exact.
    \end{inrem*}
  \item
    Let $\ccat$ be the category of $R$-modules of finite length.
    Consider the set $\Set{L_i\given i\in I}$ of representatives of isomorphism classes of irreducible objects.
    Then for any $I'\sse I$, the category
    \[
      \ecat_{I'}
      \defo
      \Set{
        M\in \ccat
        \given
        \text{$M$ has only composition factors isomorphic to $L_i$ for some $i \in I'$}
      }.
    \]
  \end{enumerate}
\end{example}

\begin{example}[{\cite[Thm.2.6]{dyckerhoff-lectures-on-hall-algebras}}]
  Let $\ecat$ be a finitary\footnote{i.e. for all $x,y\in \ecat$, both $\hom(x,y)$ and $\ext(x,y)$ are finite sets} proto-exact category.
  Then its Hall algebra is the free abelian group
  \[
    \hall(\ecat)
    =
    \bigoplus_{[M] \in \pi_{0}(\core(\ecat))}
    \zz [M],
  \]
  with composition
  \[
    [N]\cdot [L]
    =
    \sum_{[M] \in \pi_0(\core(ecat))}
    g_{N,L}^{M}
    [M],
  \]
  where $g_{N,L}^{M}$ is the number of extensions isomorphic to $L \to M \to N$.
\end{example}

\section{The Waldhausen $S$-construction}

\begin{defn}
  Let $\ccat$ be a category.
  Its \emph{arrow category} is defined as
  \[
    \arcat(\ccat)\defo \fun(\Delta^1,\ccat),
  \]
  where we write $\Delta^1$ for the category associated to the poset $[1] = \Set{0 \leq 1}$.
\end{defn}
This comes from visualizing $\Delta^1$ as $\bullet \to \bullet$, so a functor $\Delta^1\to \ccat$ corresponds to the choice of two objects in $\ccat$ with a morphism between them.
\begin{example}
  Let $\Delta^n$ be the category associated to the poset $[n]$.
  Then we have
  \[
    \arcat(\Delta^n)
    =
    \Set{(0 \leq i \leq j \leq n)},
  \]
  with the order
  \[
    (i\leq j) \leq (k\leq l)
    ~\text{iff}~
    i\leq k \text{ and } j \leq l.
  \]
  So we can visualize the diagram category $\arcat(\Delta^n)$ as
	\begin{center}
		\begin{tikzpicture}[x=-1cm,y=-1cm,line cap=round]

			\fill (0,0) circle (0.5ex) coordinate (00);
			\fill (1,0) circle (0.5ex) coordinate (10);
			\fill (2,0) circle (0.5ex) coordinate (20);
			\fill (3,0) circle (0.5ex) coordinate (30);
			\fill (4,0) circle (0.5ex) coordinate (40);
			\fill (5,0) circle (0.5ex) coordinate (50);
			\fill (6,0) circle (0.5ex) coordinate (60);
			\fill (0,1) circle (0.5ex) coordinate (01);
			\fill (1,1) circle (0.5ex) coordinate (11);
			\fill (2,1) circle (0.5ex) coordinate (21);
			\fill (3,1) circle (0.5ex) coordinate (31);
			\fill (4,1) circle (0.5ex) coordinate (41);
			\fill (5,1) circle (0.5ex) coordinate (51);
			\fill (0,2) circle (0.5ex) coordinate (02);
			\fill (1,2) circle (0.5ex) coordinate (12);
			\fill (2,2) circle (0.5ex) coordinate (22);
			\fill (3,2) circle (0.5ex) coordinate (32);
			\fill (4,2) circle (0.5ex) coordinate (42);
			\fill (0,3) circle (0.5ex) coordinate (03);
			\fill (1,3) circle (0.5ex) coordinate (13);
			\fill (2,3) circle (0.5ex) coordinate (23);
			\fill (3,3) circle (0.5ex) coordinate (33);
			\fill (0,4) circle (0.5ex) coordinate (04);
			\fill (1,4) circle (0.5ex) coordinate (14);
			\fill (2,4) circle (0.5ex) coordinate (24);
			\fill (0,5) circle (0.5ex) coordinate (05);
			\fill (1,5) circle (0.5ex) coordinate (15);
			\fill (0,6) circle (0.5ex) coordinate (06);
			\node[below left=0.5ex] at (60) {\scriptsize $(0\leq 0)$};
			\node[above=1ex,shift={(0,0)}] at (50) {\scriptsize $(0\leq 1)$};
			\node[above=1ex,shift={(0,0)}] at (40) {\scriptsize $(0\leq 2)$};
			\node[above=1ex,shift={(0,0)}] at (30) {\scriptsize $(0\leq 3)$};
			\node[above=1ex,shift={(0,0)}] at (20) {\scriptsize $(0\leq 4)$};
			\node[above=1ex,shift={(-0.275,0)}] at (10) {\scriptsize $(0\leq n-1)$};
			\node[right=1ex] at (00) {\scriptsize $(0\leq n)$};
			\node[right=1ex] at (01) {\scriptsize $(1\leq n)$};
			\node[right=1ex] at (02) {\scriptsize $(2\leq n)$};
			\node[right=1ex] at (03) {\scriptsize $(3\leq n)$};
			\node[right=1ex] at (04) {\scriptsize $(n-2\leq n)$};
			\node[right=1ex] at (05) {\scriptsize $(n-1\leq n)$};
			\node[right=1ex] at (06) {\scriptsize $(n\leq n)$};
			\node[below left=0.5ex] at (51) {\scriptsize $(1\leq 1)$};
			\node[below left=0.5ex] at (42) {\scriptsize $(2\leq 2)$};
			\node[below left=0.5ex] at (33) {\scriptsize $(3\leq 3)$};
			\node[below left=0.5ex] at (24) {\scriptsize $(n-2\leq n-2)$};
			\node[below left=0.5ex] at (15) {\scriptsize $(n-1\leq n-1)$};
			\draw[-to,shorten <=1.25ex, shorten >=1.75ex] (60) to (50);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (50) to (40);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (40) to (30);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (30) to (20);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (10) to (00);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (51) to (41);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (41) to (31);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (31) to (21);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (11) to (01);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (42) to (32);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (32) to (22);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (12) to (02);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (33) to (23);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (13) to (03);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (24) to (14);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (14) to (04);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (15) to (05);
			\draw[-to,shorten <=1.75ex, shorten >=1.25ex] (50) to (51);
			\draw[-to,shorten <=1.75ex, shorten >=1.25ex] (40) to (41);
			\draw[-to,shorten <=1.75ex, shorten >=1.25ex] (30) to (31);
			\draw[-to,shorten <=1.75ex, shorten >=1.25ex] (20) to (21);
			\draw[-to,shorten <=1.75ex, shorten >=1.25ex] (10) to (11);
			\draw[-to,shorten <=1.75ex, shorten >=1.25ex] (00) to (01);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (41) to (42);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (31) to (32);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (21) to (22);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (11) to (12);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (01) to (02);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (32) to (33);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (22) to (23);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (12) to (13);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (02) to (03);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (14) to (15);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (04) to (05);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (05) to (06);
			\path (20) to node[pos=0.5] {$\dotso$} (10);
			\path (21) to node[pos=0.5] {$\dotso$} (11);
			\path (22) to node[pos=0.5] {$\dotso$} (12);
			\path (23) to node[pos=0.5] {$\dotso$} (13);
			\path (23) to node[pos=0.5,sloped] {$\dotso$} (24);
			\path (33) to node[pos=0.5,sloped] {$\dotso$} (24);
			\path (13) to node[pos=0.5,sloped] {$\dotso$} (14);
			\path (03) to node[pos=0.5,sloped] {$\dotso$} (04);
		\end{tikzpicture}
	\end{center}
with the upper row consisting of $(n+1)$ bullets.
If $\ccat$ is any category, then giving a functor $\arcat(\Delta^n)\to \ccat$ amounts to specifying objects $F(i,j)_{0\leq i\leq j \leq n}$ such that the diagram of squares as above is commutative.
\end{example}

\begin{defn}
  Let $(\ecat,\adm,\ade)$ be a proto-exact category.
  Let $\ww_n(\she) \sse \fun(\arcat(\Delta^n),\she)$ be the full subcategory formed by diagrams as above that satisfy additionally
  \begin{enumerate}
  \item
    For every $0 \leq i \leq n$, we have $F(i,i) \cong 0$;
  \item
    All horizontal morphisms are in $\adm$, and all vertical morphisms are in $\ade$;
  \item
    Each square in the diagram is bicartesian.
  \end{enumerate}
  We denote by $\sw_n(\ccat)$ the core of $\ww_n(\ccat)$.
\end{defn}

\begin{example}
  We have $\sw_1(\she) = \core(\adm) = \core(\ecat)$.
  We can think of elements of $\sw_2(\she)$ as \enquote{admissible $1$-filtrations}, i.e. an admissible monomorphism $a\hookrightarrow b$ in $\adm$.
  However, we also keep track of the associated quotient-object, as in the diagram
  \[
    \begin{tikzcd}
      0
      \ar{r}
      &
      a
      \ar[hookrightarrow]{r}
      \ar[twoheadrightarrow]{d}
      &
      b
      \ar[twoheadrightarrow]{d}
      \\
      &
      0
      \ar[hookrightarrow]{r}
      &
      b/a
    \end{tikzcd}
  \]
\end{example}
Let us record this observation as a lemma:
\begin{lem}
  The functor $\sw_n(\ccat) \to \fun(\Delta^{n-1},\adm)$ associating to $F$ the subdiagram
  \[
    \begin{tikzcd}[column sep = small]
      F(0,1)
      \ar{r}
      &
      F(0,2)
      \ar{r}
      &
      \ldots
      \ar{r}
      &
      F(0,n)
    \end{tikzcd}
  \]
  is an equivalence of categories.
\end{lem}
\begin{proof}
  Iterate the above process.
\end{proof}
So we can more accurately visualize $\sw_n$ as
	\begin{center}
		\begin{tikzpicture}[x=-1cm,y=-1cm,line cap=round]

			\draw[line width=2.5ex,yellow!42!white] (5,0) to (0,0);
			\fill[purple!42!white] (6,0) circle (1.25ex);
			\fill[purple!42!white] (5,1) circle (1.25ex);
			\fill[purple!42!white] (4,2) circle (1.25ex);
			\fill[purple!42!white] (3,3) circle (1.25ex);
			\fill[purple!42!white] (2,4) circle (1.25ex);
			\fill[purple!42!white] (1,5) circle (1.25ex);
			\fill[purple!42!white] (0,6) circle (1.25ex);
			\fill (0,0) circle (0.5ex) coordinate (00);
			\fill (1,0) circle (0.5ex) coordinate (10);
			\fill (2,0) circle (0.5ex) coordinate (20);
			\fill (3,0) circle (0.5ex) coordinate (30);
			\fill (4,0) circle (0.5ex) coordinate (40);
			\fill (5,0) circle (0.5ex) coordinate (50);
			\fill (6,0) circle (0.5ex) coordinate (60);
			\fill (0,1) circle (0.5ex) coordinate (01);
			\fill (1,1) circle (0.5ex) coordinate (11);
			\fill (2,1) circle (0.5ex) coordinate (21);
			\fill (3,1) circle (0.5ex) coordinate (31);
			\fill (4,1) circle (0.5ex) coordinate (41);
			\fill (5,1) circle (0.5ex) coordinate (51);
			\fill (0,2) circle (0.5ex) coordinate (02);
			\fill (1,2) circle (0.5ex) coordinate (12);
			\fill (2,2) circle (0.5ex) coordinate (22);
			\fill (3,2) circle (0.5ex) coordinate (32);
			\fill (4,2) circle (0.5ex) coordinate (42);
			\fill (0,3) circle (0.5ex) coordinate (03);
			\fill (1,3) circle (0.5ex) coordinate (13);
			\fill (2,3) circle (0.5ex) coordinate (23);
			\fill (3,3) circle (0.5ex) coordinate (33);
			\fill (0,4) circle (0.5ex) coordinate (04);
			\fill (1,4) circle (0.5ex) coordinate (14);
			\fill (2,4) circle (0.5ex) coordinate (24);
			\fill (0,5) circle (0.5ex) coordinate (05);
			\fill (1,5) circle (0.5ex) coordinate (15);
			\fill (0,6) circle (0.5ex) coordinate (06);
			\node[below left=0.5ex] at (60) {\scriptsize $(0\leq 0)$};
			\node[above=1ex,shift={(0,0)}] at (50) {\scriptsize $(0\leq 1)$};
			\node[above=1ex,shift={(0,0)}] at (40) {\scriptsize $(0\leq 2)$};
			\node[above=1ex,shift={(0,0)}] at (30) {\scriptsize $(0\leq 3)$};
			\node[above=1ex,shift={(0,0)}] at (20) {\scriptsize $(0\leq 4)$};
			\node[above=1ex,shift={(-0.275,0)}] at (10) {\scriptsize $(0\leq n-1)$};
			\node[right=1ex] at (00) {\scriptsize $(0\leq n)$};
			\node[right=1ex] at (01) {\scriptsize $(1\leq n)$};
			\node[right=1ex] at (02) {\scriptsize $(2\leq n)$};
			\node[right=1ex] at (03) {\scriptsize $(3\leq n)$};
			\node[right=1ex] at (04) {\scriptsize $(n-2\leq n)$};
			\node[right=1ex] at (05) {\scriptsize $(n-1\leq n)$};
			\node[right=1ex] at (06) {\scriptsize $(n\leq n)$};
			\node[below left=0.5ex] at (51) {\scriptsize $(1\leq 1)$};
			\node[below left=0.5ex] at (42) {\scriptsize $(2\leq 2)$};
			\node[below left=0.5ex] at (33) {\scriptsize $(3\leq 3)$};
			\node[below left=0.5ex] at (24) {\scriptsize $(n-2\leq n-2)$};
			\node[below left=0.5ex] at (15) {\scriptsize $(n-1\leq n-1)$};
			\node at (4.5,0.5) {$\pushoutsign$};
			\node at (3.5,0.5) {$\pushoutsign$};
			\node at (2.5,0.5) {$\pushoutsign$};
			\node at (0.5,0.5) {$\pushoutsign$};
			\node at (3.5,1.5) {$\pushoutsign$};
			\node at (2.5,1.5) {$\pushoutsign$};
			\node at (0.5,1.5) {$\pushoutsign$};
			\node at (2.5,2.5) {$\pushoutsign$};
			\node at (0.5,2.5) {$\pushoutsign$};
			\node at (0.5,4.5) {$\pushoutsign$};
			\draw[-to,shorten <=1.25ex, shorten >=1.75ex] (60) to (50);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (50) to (40);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (40) to (30);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (30) to (20);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (10) to (00);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (51) to (41);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (41) to (31);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (31) to (21);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (11) to (01);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (42) to (32);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (32) to (22);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (12) to (02);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (33) to (23);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (13) to (03);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (24) to (14);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (14) to (04);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (15) to (05);
			\draw[-to,shorten <=1.75ex, shorten >=1.25ex] (50) to (51);
			\draw[-to,shorten <=1.75ex, shorten >=1.25ex] (40) to (41);
			\draw[-to,shorten <=1.75ex, shorten >=1.25ex] (30) to (31);
			\draw[-to,shorten <=1.75ex, shorten >=1.25ex] (20) to (21);
			\draw[-to,shorten <=1.75ex, shorten >=1.25ex] (10) to (11);
			\draw[-to,shorten <=1.75ex, shorten >=1.25ex] (00) to (01);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (41) to (42);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (31) to (32);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (21) to (22);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (11) to (12);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (01) to (02);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (32) to (33);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (22) to (23);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (12) to (13);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (02) to (03);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (14) to (15);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (04) to (05);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (05) to (06);
			\path (20) to node[pos=0.5] {$\dotso$} (10);
			\path (21) to node[pos=0.5] {$\dotso$} (11);
			\path (22) to node[pos=0.5] {$\dotso$} (12);
			\path (23) to node[pos=0.5] {$\dotso$} (13);
			\path (23) to node[pos=0.5,sloped] {$\dotso$} (24);
			\path (33) to node[pos=0.5,sloped] {$\dotso$} (24);
			\path (13) to node[pos=0.5,sloped] {$\dotso$} (14);
			\path (03) to node[pos=0.5,sloped] {$\dotso$} (04);
			\draw[dotted] (-5cm-1.25ex,0) to[out=270,in=180] (5,-1.25ex) to (0,-1.25ex) to[out=0,in=270] (1.25ex,0) to[out=90,in=0] (0,1.25ex) to (5,1.25ex) to[out=180,in=90] cycle;
		\end{tikzpicture}
	\end{center}
  with the yellow part corresponding to the \enquote{essential} part of the diagram, and the purple dots corresponding to the zero-objects.

  \begin{prop}
    Let $\she$ be a proto-exact category.
    Then the association $\Delta^n \mapsto \sw_n(\she)$ defines a simplicial groupoid
    \[
      \sw_{\bullet}(\ecat)
      \mc
      \Delta^{\op}
      \to
      \grpd.
    \]
  \end{prop}
  \begin{proof}
    It suffices to observe that the various $\adm_n$ are stable under face- and degeneracy maps.
    But this is ok, since all of them are stable under composition and they contain all identities.
  \end{proof}
  \begin{theorem}
    Let $\ecat$ be a proto-exact category.
    Then the simplicial groupoid $\sw_{\bullet}(\ecat)$ is 2-Segal.
  \end{theorem}
  \begin{rem}
    Before we sketch the proof, let us note the slight diversion from the previous talk (the same is done in \cite{dyckerhoff-lectures-on-hall-algebras}, and it is allowed that we do this by \cite[Prop.1.3.8]{dyckerhoff-kapranov}).
    Namely, we talk about 2-Segal simplicial groupoids, instead of 2-Segal topological spaces.
    The definition is analogous, namely if $X_{\bullet}$ is a simplicial groupoid, we require that for every $n \geq 2$, and diagonal subdivision of the regular $(n+1)$-gon $P$ with labels $\Set{0,1,\ldots,i,j,j+1,\ldots,n}$ and $\Set{i,i+1,\ldots,j}$, the diagram
    \[
      \begin{tikzcd}
        X_{\Set{0,1,\ldots,n}}
        \ar{r}
        \ar{d}
        &
        X_{\Set{0,1,\ldots,i,j,\ldots,n}}
        \ar{d}
        \\
        X_{\Set{i,i+1,\ldots,j}}
        \ar{r}
        &
        X_{\Set{i,j}}
      \end{tikzcd}
    \]
    is a 2-cartesian square of groupoids\footnote{The objects of a 2-pullback of groupoids can be constructed similar as the objects of the fiber product of the objects, only that the condition in the fiber of being equal is replaced by the condition of being isomorphic. We will mention below that we do not to worry about this.}, and moreover, the square
    \[
      \begin{tikzcd}
        X_{\Set{0,1,\ldots,n-1}}
        \ar{r}
        \ar{d}[left]{\sigma_i}
        &
        X_{\Set{i}}
        \ar{d}
        \\
        X_{\Set{0,1,\ldots,n}}
        \ar{r}
        &
        X_{\Set{i,i+1}}
      \end{tikzcd}
    \]
   is 2-cartesian.
  \end{rem}
  \begin{lem}
    Let
    \[
      \begin{tikzcd}
        D'
        \ar{r}
        \ar{d}
        &
        C'
        \ar{d}
        \\
        D
        \ar{r}[below]{F}
        &
        C
      \end{tikzcd}
      \]
      be a commutative square of groupoids, that is a cartesian square on underlying sets.
      Assume that $F$ is an isofibration.\footnote{If $a\in D$ and $b\in C$ with $\varphi\mc F(a)\to b$, there exists an isomorphism $\tilde{\varphi}\mc a \to \tilde{b}$ in $D$ with $F(\tilde{\varphi}) = \varphi$.}
      Then the square is $2$-cartesian.
  \end{lem}
  \begin{proof}[Sketch of proof of the theorem]
    First, note that
    \[
  \sw_{\Set{i,i+1,\ldots,j}}
    \to
        \sw_{\Set{i,j}}
    \]
    is an isofibration, so we do not have to care about \enquote{higher coherences} for the moment.
    Now the functor
    \[
      \sw_{\Set{0,\ldots, n}}
      \to
        \sw_{\Set{0,1,\ldots,i,j,\ldots,n}}
        \times_{\sw_{\Set{i,j}}}
        \sw_{\Set{i,i+1,\ldots,j}}
      \]
      is given by removing objects in a diagram of $\sw_n(\she)$ whose indices correspond to diagonals of $P_{n}$ crossing the diagonal $(i,j)$, who can again uniquely be filled in with pullbacks and pushouts, and the resulting diagrams will again be bicartesian, because we have pasting laws.
      Let us illustrate how this works for the triangulation of the square, were we indicate the missing diagonal by drawing it bold:
      \begin{center}
        \begin{tikzpicture}
\begin{scope}[decoration={
    }
      ]
        \draw[postaction = {decorate}](0,0) node [anchor = north] {\tiny$0$} -- (1,0) node [anchor = north]{\tiny$1$};
      \draw[postaction = {decorate}](0,0) -- (0,1) node [anchor = south]{\tiny$3$};
      \draw[postaction = {decorate}](1,0) -- (1,1) node[anchor = south]{\tiny$2$};
      \draw[postaction = {decorate}](0,1) -- (1,1);
      \draw[postaction = {decorate}](0,0) -- (1,1);
      \draw[thick] (0,1) -- (1,0);
      \end{scope}
    \path[fill = blue, fill opacity = .3] (0,0) -- (1,1) -- (0,1) -- (0,0);
    \path[fill = red, fill opacity = .3] (0,0) -- (1,0) -- (1,1) -- (0,0);
    \end{tikzpicture}
    \end{center}
    So we are faced with solving a lifting problem of the form
    \[
    \begin{tikzcd}
      0
      \ar{r}
      &
      a
      \ar{d}
      \ar{r}
      &
      b
      \ar{r}
      \ar{d}
      &
      c
      \ar[bend left = 30]{dd}
      \\
      &
      0
      \ar{r}
      &
      a'
      \ar{d}
      &
      ?
      \\
      &
      &
      0
      \ar{r}
      &
      a''
      \ar{d}
      \\
      &
      &
      &
      0
    \end{tikzcd}
    \]
    This can be done as follows:
    First, we complete
    \[
      \begin{tikzcd}
        b
        \ar{r}
        \ar{d}
        &
        c
        \ar[dashed]{d}
        \\
        a'
        \ar[dashed]{r}
        &
        b'
      \end{tikzcd}
    \]
    to a bicartesian square.
    Then, in the second step, we can use the universal property of $b'$ to get the desired morphism $b' \to a''$.
    Pasting for bicartesian squares ensures that
    \[
      \begin{tikzcd}
        a'
        \ar{r}
        \ar{d}
        &
        b'
        \ar{d}
        \\
        0
        \ar{r}
        &
        a'
      \end{tikzcd}
    \]
      is bicartesian too.
      To give a slightly more extravagant example, consider the case $n = 5$, and the diagonal $(i,j)$ in the regular $6$-gon as pictured below, where again, the crossing diagonals are indicated in bold:

      \begin{center}
\begin{tikzpicture}
% create the node
\node[draw=black,minimum size=2cm,regular polygon,regular polygon sides=6] (a) {};
% draw a black dot in each vertex
  \node at (a.corner 1) [anchor = south] {\tiny$3$};
  \node at (a.corner 2) [anchor = south] {\tiny$4$};
  \node at (a.corner 3) [anchor = east] {\tiny$5$};
  \node at (a.corner 4) [anchor = north] {\tiny$0$};
  \node at (a.corner 5) [anchor = north] {\tiny$1$};
  \node at (a.corner 6) [anchor = west] {\tiny$2$};

  \draw (a.corner 5) -- (a.corner 2);
  \draw[thick] (a.corner 4) -- (a.corner 1);
  \draw[thick] (a.corner 3) -- (a.corner 6);
  \draw[thick] (a.corner 3) -- (a.corner 1);
  \draw[thick] (a.corner 4) -- (a.corner 6);

  \path[fill = blue, fill opacity = .3] (a.corner 4) -- (a.corner 5) -- (a.corner 2) -- (a.corner 3) -- (a.corner 4);
  \path[fill = red, fill opacity = .3] (a.corner 5) -- (a.corner 6) -- (a.corner 1) -- (a.corner 2) -- (a.corner 5);
\end{tikzpicture}
\end{center}
This corresponds to adding the dots indicated in green and the missing morphisms in the picture below:

	\begin{center}
		\begin{tikzpicture}[x=-1cm,y=-1cm,line cap=round]
			\fill[green!42!white] (4,0) circle (1.25ex);
			\fill[green!42!white] (3,0) circle (1.25ex);
			\fill[green!42!white] (1,3) circle (1.25ex);
			\fill[green!42!white] (1,2) circle (1.25ex);
			\fill (1,0) circle (0.5ex) coordinate (10);
			\fill (2,0) circle (0.5ex) coordinate (20);
			\fill (3,0) circle (0.5ex) coordinate (30);
			\fill (4,0) circle (0.5ex) coordinate (40);
			\fill (5,0) circle (0.5ex) coordinate (50);
			\fill (6,0) circle (0.5ex) coordinate (60);
			\fill (1,1) circle (0.5ex) coordinate (11);
			\fill (2,1) circle (0.5ex) coordinate (21);
			\fill (3,1) circle (0.5ex) coordinate (31);
			\fill (4,1) circle (0.5ex) coordinate (41);
			\fill (5,1) circle (0.5ex) coordinate (51);
			\fill (1,2) circle (0.5ex) coordinate (12);
			\fill (2,2) circle (0.5ex) coordinate (22);
			\fill (3,2) circle (0.5ex) coordinate (32);
			\fill (4,2) circle (0.5ex) coordinate (42);
			\fill (1,3) circle (0.5ex) coordinate (13);
			\fill (2,3) circle (0.5ex) coordinate (23);
			\fill (3,3) circle (0.5ex) coordinate (33);
			\fill (1,4) circle (0.5ex) coordinate (14);
			\fill (2,4) circle (0.5ex) coordinate (24);
			\fill (1,5) circle (0.5ex) coordinate (15);
%			\node[above=1ex,shift={(0,0)}] at (50) {\scriptsize $(0\leq 1)$};
%			\node[above=1ex,shift={(0,0)}] at (40) {\scriptsize $(0\leq 2)$};
%			\node[above=1ex,shift={(0,0)}] at (30) {\scriptsize $(0\leq 3)$};
%			\node[above=1ex,shift={(0,0)}] at (20) {\scriptsize $(0\leq 4)$};
			\node[right=1ex] at (10) {\scriptsize $(0\leq 5)$};
%			\node[right=1ex] at (11) {\scriptsize $(1\leq 5)$};
%			\node[right=1ex] at (12) {\scriptsize $(2\leq 5)$};
%			\node[right=1ex] at (13) {\scriptsize $(3\leq 5)$};
%			\node[right=1ex] at (14) {\scriptsize $(4\leq 5)$};
			\node[right=1ex] at (15) {\scriptsize $(5\leq 5)$};
			\node[below left=0.5ex] at (60) {\scriptsize $(0\leq 0)$};
			\node[below left=0.5ex] at (51) {\scriptsize $(1\leq 1)$};
			\node[below left=0.5ex] at (42) {\scriptsize $(2\leq 2)$};
			\node[below left=0.5ex] at (33) {\scriptsize $(3\leq 3)$};
			\node[below left=0.5ex] at (24) {\scriptsize $(4\leq 4)$};
			\draw[-to,shorten <=1.25ex, shorten >=1.75ex] (60) to (50);
%			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (50) to (40);
%			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (40) to (30);
%			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (30) to (20);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (51) to (41);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (41) to (31);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (31) to (21);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (42) to (32);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (32) to (22);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (33) to (23);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (24) to (14);
			\draw[-to,shorten <=1.75ex, shorten >=1.25ex] (50) to (51);
%			\draw[-to,shorten <=1.75ex, shorten >=1.25ex] (40) to (41);
%			\draw[-to,shorten <=1.75ex, shorten >=1.25ex] (30) to (31);
			\draw[-to,shorten <=1.75ex, shorten >=1.25ex] (20) to (21);
			\draw[-to,shorten <=1.75ex, shorten >=1.25ex] (10) to (11);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (41) to (42);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (31) to (32);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (21) to (22);
%			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (11) to (12);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (32) to (33);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (22) to (23);
%			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (12) to (13);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (14) to (15);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (20) to (10);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (21) to (11);
%			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (22) to (12);
%			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (23) to (13);

			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (23) to (24);
%			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (13) to (14);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (50.north) to [out=30,in=150] (20.north);
			\draw[-to,shorten <=1.25ex, shorten >=1.25ex] (11.west) to [out=-30,in=30] (14.west);
Again, we can do this.
		\end{tikzpicture}
	\end{center}

  \end{proof}
  \begin{rem}
    This proof becomes more precise if we use that we only need to check the 2-Segal condition for $i=0$ or $j = n$ (\cite[Prop.2.3.2]{dyckerhoff-kapranov}).
    In that case, it suffices in the $i= 0$ case to show that the maps
    \[
      \adm_{n}
      \to
      \adm_j
      \times_{\sw_{\Set{0,j}}}^{(2)}
      \adm_{n-j+1}    \]
    induced by splitting up the filtration at the $j$-th point are equivalences.
    But this only means that two sequences
    \[
      \Set{F(0,1)\hookrightarrow\ldots \hookrightarrow F(0,j)}
      \in
      \adm_{j}
      ,~
      \Set{F'(0,j)\hookrightarrow\ldots \hookrightarrow F'(0,n)} \in \adm_{n-j +1}
    \]
    together with an isomorphism $F(0,j) \to F'(0,j)$ combine to a canonical object of $\adm_n$, which is ok.
    The condition for $j = n$ can be shown similar, but now with $\ade$ instead of $\adm$.
  \end{rem}
  \section{Waldhausen $S$-construction in relation to other concepts}

  \begin{example}
    $\pi_1(\sw_{\bullet}(\modcat(R)^{\omega}))$ recovers the usual $K_0(R)$.
    Also, $\sw_{\bullet}(\modcat(R)^{\omega})$ is not 1-Segal if there are non-split extensions, because the Segal map
    \[
      \sw_{2}
      \to
      \sw_{1}
      \times
      \sw_{1}
    \]
    maps the extension $(a\hookrightarrow b \twoheadrightarrow a')$ to the pair $(a,a')$.
  \end{example}

  \subsection*{Hall algebras}
    Let $\sw_{\bullet}$ be a 2-Segal simplicial groupoid, with $\sw_0= \mathrm{pt}$.
    \begin{prop}
      Consider the groupoid $\sw_1$. Then $\sw_1$, together with the span $\mu$
      \[
        \begin{tikzcd}
          &
          \sw_{\Set{0,1,2}}
          \ar{ld}
          \ar{rd}
          &
          \\
          \sw_{\Set{0,1}}
          \times
          \sw_{\Set{1,2}}
          &
          &
          \sw_{\Set{0,2}}
        \end{tikzcd}
      \]
      as multiplication, is an algebra object in the span-category $\spancat(\grpd)$.
    \end{prop}
    \begin{proof}[Sketch of proof]
      The idea is that both compositions $\mu\circ(\mu\tensor \id)$ and $(\mu \tensor \id) \circ \mu$ can be represented by the same cartesian diagram, which can be verified using one of the 2-Segal maps.
    \end{proof}
    Let $\spancat^f(\grpd)$ be the thick subcategory with morphisms given by spans $\shg' \xleftarrow{L} \shg \xrightarrow{R} \shg''$ such that $\pi_{0}L$ has finite fibers and $R$ is locally finite, in the sense that its restriction to every connected component has finite 2-fibers.
    \begin{prop}
      If $\she$ is a finitary proto-exact category, then the Hall-algebra $\hall(\she)$ is isomorphic to the opposite of the algebra $\shf(\sw_1(\ecat))$, where $\shf$ is the monoidal functor $\spancat^f(\grpd) \to \vectcat_{\rat}$, that maps a groupoid $\shg$ to the $\rat$-linear space of functions $\pi_{0}(\shg) \to \rat$ that are constant on connected components, and non-zero for only finitely many of them.
    \end{prop}
 \printbibliography
%\listoftheorems[ignoreall,
%show={todo, danger}]
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
